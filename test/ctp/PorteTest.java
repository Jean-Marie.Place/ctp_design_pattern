package ctp;

import org.junit.Test;

import static ctp.Porte.Etat.*;
import static org.junit.Assert.*;

public class PorteTest {

    @Test
    public void initialement_fermee() {
        Porte porte = new Porte();
        assertEquals(FERMEE, porte.getEtat());
    }

    @Test
    public void demande_d_ouverture() {
        Porte porte = new Porte();
        porte.setEtat(FERMEE);
        assertTrue(porte.bouton());
        assertEquals(OUVERTURE, porte.getEtat());
    }

    @Test
    public void butee_fermee() {
        Porte porte = new Porte();
        porte.setEtat(FERMEE);
        assertFalse(porte.butee());
        assertEquals(FERMEE, porte.getEtat());
    }

    @Test
    public void demande_de_fermeture() {
        Porte porte = new Porte();
        porte.setEtat(OUVERTE);
        assertTrue(porte.bouton());
        assertEquals(FERMETURE, porte.getEtat());
    }

    @Test
    public void butee_ouverte() {
        Porte porte = new Porte();
        porte.setEtat(OUVERTE);
        assertFalse(porte.butee());
        assertEquals(OUVERTE, porte.getEtat());
    }

    @Test
    public void contrordre_fermeture() {
        Porte porte = new Porte();
        porte.setEtat(FERMETURE);
        assertTrue(porte.bouton());
        assertEquals(OUVERTURE, porte.getEtat());
    }

    @Test
    public void fin_fermeture() {
        Porte porte = new Porte();
        porte.setEtat(FERMETURE);
        assertTrue(porte.butee());
        assertEquals(FERMEE, porte.getEtat());
    }

    @Test
    public void contrordre_ouverture() {
        Porte porte = new Porte();
        porte.setEtat(OUVERTURE);
        assertTrue(porte.bouton());
        assertEquals(FERMETURE, porte.getEtat());
    }

    @Test
    public void fin_ouverture() {
        Porte porte = new Porte();
        porte.setEtat(OUVERTURE);
        assertTrue(porte.butee());
        assertEquals(OUVERTE, porte.getEtat());
    }
}